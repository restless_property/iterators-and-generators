"""
This module carries unittests on the 'frange.py' module.
"""


import itertools
import unittest
#
import frange  # pylint: disable=E0401


class MainTest(unittest.TestCase):
    """
    This class runs tests on the execution
    of the 'frange.py' module.

    Methods:
    test_frange(): tests arbitrary values for equality
    """

    def test_frange(self):
        """
        This method tests the results of the work
        of the class 'Frange' from the module 'frange.py'.
        """
        tested = list(frange.Frange(5))
        self.assertEqual(tested, [0, 1, 2, 3, 4])
        #
        tested = list(frange.Frange(2, 5))
        self.assertEqual(tested, [2, 3, 4])
        #
        tested = list(frange.Frange(2, 10, 2))
        self.assertEqual(tested, [2, 4, 6, 8])
        #
        tested = list(frange.Frange(10, 2, -2))
        self.assertEqual(tested, [10, 8, 6, 4])
        #
        tested = list(frange.Frange(1, 5))
        self.assertEqual(tested, [1, 2, 3, 4])
        #
        tested = list(frange.Frange(0, 5))
        self.assertEqual(tested, [0, 1, 2, 3, 4])
        #
        tested = list(frange.Frange(0, 0))
        self.assertEqual(tested, [])
        #
        tested = list(frange.Frange(100, 0))
        self.assertEqual(tested, [])
        #
        tested = list(itertools.islice(frange.Frange(0, float(10 ** 10), 1.0), 0, 4))
        self.assertEqual(tested, [0, 1.0, 2.0, 3.0])
        #
        with self.assertRaises(TypeError):
            frange.Frange('3', None, True)
        #
        with self.assertRaises(StopIteration):
            tested = frange.Frange(1, 6, 1)
            for _ in range(6):
                next(tested)


if __name__ == '__main__':
    unittest.main()
