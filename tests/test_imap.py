"""
This module carries unittests on the 'imap.py' module.
"""

import math
import unittest
#
import imap  # pylint: disable=E0401


class MainTest(unittest.TestCase):
    """
    This class runs tests on the execution
    of the 'imap.py' module.

    Methods:
    trst_imap(): tests arbitrary values for equality
    """

    def test_imap(self):
        """
        This method tests the results of the work
        of the class 'Imap' from the module 'imap.py'.
        """
        tested = list(imap.Imap(lambda num: num ** num, [1, 2, 3]))
        self.assertEqual(tested, [1, 4, 27])
        #
        tested = tuple(imap.Imap(lambda char: char * 2, 'abcdefg'))
        self.assertEqual(tested, ('aa', 'bb', 'cc', 'dd', 'ee', 'ff', 'gg'))
        #
        tested = list(imap.Imap(lambda num: num + 1, [0, 0.0, 0+0j]))
        self.assertTrue(tested[0])
        self.assertTrue(tested[1])
        self.assertTrue(tested[2])
        #
        tested = list(imap.Imap(lambda char: char.upper(), 'abcdefg'))
        self.assertIn('E', tested)
        #
        tested = list(imap.Imap(math.sqrt, [1, 4, 9, 16, 25]))
        self.assertIsInstance(tested[0], float)
        self.assertIsInstance(tested[1], float)
        self.assertIsInstance(tested[2], float)
        self.assertIsInstance(tested[3], float)
        self.assertIsInstance(tested[4], float)
        #
        with self.assertRaises(StopIteration):
            tested = imap.Imap(lambda elem: 'Element', [10, 23.5, 'test', (45, False), {}])
            for _ in range(6):
                next(tested)


if __name__ == '__main__':
    unittest.main()
