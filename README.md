### **PERSONAL RANGE AND MAP**
    
    
        This project provides a personal realization of such Python classes:
        
            1. class Frange(stop) / class Frange(start, stop, [step]).
               The class resembles built-in Python class 'range', able to accept floats as arguements.
            2. class Imap(function, iterable)
               The class simulates the familiar work of the built-in Python class 'map'.
               
        Separate modules with unittests are provided within the 'tests' folder.
