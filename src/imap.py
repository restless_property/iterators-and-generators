"""
This module is a personal realization for 'Python' class 'map'.
"""

from collections.abc import Iterable
import string


class Imap:
    """
    The class "Imap" emulates the work
    of the built-in Python class "map".
    """

    def __init__(self, function, iterable):
        """
        The constructor of the class.

        :param function: (func) a fucntion to apply
                                to each consequent element of the iterable
        :param iterable: a collection or a sequence
                         which element are to be changed with the function

        Introduced fields:
        self.pointer: (int) the indicator of a current element
                            called by the iterator
        """
        self.function = function
        self.iterable = iterable
        self.pointer = 0

        # if not isinstance(self.iterable, (str, list, tuple, set, frozenset, dict,
        #                                   bytes, bytearray, range)):
        if not isinstance(self.iterable, Iterable):
            raise TypeError(f'{type(self.iterable).__name__} object is not iterable')

    def __next__(self):
        """
        The method of the iterator
        which returns a consequent element of the sequence.

        :return element: a consequent element of the sequence
        """
        if self.pointer == len(self.iterable):
            raise StopIteration
        element = self.function(self.iterable[self.pointer])
        self.pointer += 1
        return element

    def __iter__(self):
        """
        This method of the iterable
        returns the instance of the class as an iterator.
        :return: (Imap) an instance of this class
        """
        return self


def defy_even_or_odd(element):
    """
    The function verifies
    weather an input number is odd or even

    :param element: (int/float) a number to verify
    :return number_type: (str) the type of an input number
    """
    if element % 2 == 0:
        number_type = 'Even'
    else:
        number_type = 'Odd'
    return number_type


def reverse(boolean: bool) -> bool:
    """
    The function simply inverts the value
    of a boolean object.

    :param boolean: (bool) an initial boolean value
    :return: (bool) a reversed boolean value
    """
    return not boolean


if __name__ == '__main__':
    print()
    numbers = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
    imap_num = Imap(defy_even_or_odd, numbers)
    print(imap_num)
    print(numbers)
    print(list(imap_num))
    print()
    # --- #
    characters = string.printable  # pylint: disable=C0103
    imap_char = Imap(ord, characters)
    print(imap_char)
    print(characters)
    print(list(imap_char))
    print()
    # --- #
    booleans = (True, True, False, True, False, True, False, False)
    imap_bool = Imap(reverse, booleans)
    print(imap_bool)
    print(booleans)
    print(tuple(imap_bool))
