"""
This module implies a realization for 'Python' class 'range'
which would support floating numbers as arguments.
"""
import itertools


class Frange:
    """
    The class "Frange" emulates the work
    of the built-in Python class "range",
    but supports float numbers as arguments.

    Methods:
    __calculate_ndigits: a private method;
                        computes the number of digits
                        to round a consequent element
                        of the iterable object

    """

    def __init__(self, start, stop=None, step=1):
        """
        The constructor of the class.
        :param start: (int/float) the starting edge of the sequence; inclusive
        :param stop: (int/float) the boundary edge of the sequence; exclusive
        :param step: (int/float) the step to increment/decrement the consequent element

        Introduced fields:
        self.element: (int/float) the consequent element in the range of numbers
        self.ndigits: (int) the number of digits to round a consequent element to
        """
        self.start = start
        self.stop = stop
        self.step = step
        self.element, self.ndigits = 0, 0

        if self.stop is None:
            self.stop = self.start
            self.start = 0

        if not isinstance(self.start, (int, float)) or \
                not isinstance(self.stop, (int, float)) or \
                not isinstance(self.step, (int, float)):
            raise TypeError("Input arguments cannot be interpreted as integers or floats")

    def __str__(self):
        """
        The method implicates the visual representation of an instance.
        :return: (str) textual realization of the boundary limits of a sequence
        """
        return f"frange({self.start}, {self.stop})"

    def __calculate_ndigits(self):
        """
        The method is private
        and defines the number of digits
        to round a consequent element of a sequence to.

        :return self.ndigits: (int) the number of digits to round to

        Local variables:
        string: (str) the value of the step represented as a string
        separator: (list/str) separated integer and fractional part of the step;
                              then, a string made of the fractional part of the step,
                              used to define the number of ranks to round a consequent element to
        """
        string = str(self.step)
        separator = string.split('.')
        separator.pop(0)
        separator = ''.join(separator)
        self.ndigits = len(separator)
        return self.ndigits

    def __next__(self):
        """
        The method of the iterator
        which returns a consequent element of the sequence.

        :return: (int/folat) a consequent element of the sequence (rounded)
        """
        self.__calculate_ndigits()
        if (self.step < 0 and self.start > self.stop) or (self.step > 0 and self.start < self.stop):
            self.element = self.start
            self.start += self.step
            return round(self.element, self.ndigits)
        raise StopIteration

    def __iter__(self):
        """
        This method of the iterable
        returns the instance of the class as an iterator.
        :return: (Frange) an instance of this class
        """
        return self


if __name__ == '__main__':
    frange = Frange(-3, 5.5, 2.5)
    lst = list(frange)
    print(frange)
    print(lst)
    print(lst[-1])
